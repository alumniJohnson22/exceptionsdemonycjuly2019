package com.mj3.training.exceptions;

public class InternalCrossException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InternalCrossException() {
		super();
	}

	public InternalCrossException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InternalCrossException(String arg0) {
		super(arg0);
	}

	public InternalCrossException(Throwable arg0) {
		super(arg0);
	}
	
	
	
	
	
	

}
