package com.mj3.training.exceptions;

public class InternalCrossRunTimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InternalCrossRunTimeException() {
		super();
	}

	public InternalCrossRunTimeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InternalCrossRunTimeException(String arg0) {
		super(arg0);
	}

	public InternalCrossRunTimeException(Throwable arg0) {
		super(arg0);
	}
	
	
	
	
	
	

}
