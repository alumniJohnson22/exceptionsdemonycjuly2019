package com.mj3.training.exceptions;

public class DemoAssemblyOnly {
	
	// DON'T unconditionally throw 
	// DEMO only
	public void noticeBadThingAndThrow() {
		throw new IllegalStateException("Bad thing has happened.");
	}
	
	public static void main(String[] args) {
		try {
			new DemoAssemblyOnly().noticeBadThingAndThrow();
			// dead code, because of unconditional throw
			System.out.println("Everything continues ok");
		}
		catch(IllegalStateException e) {
			// do some work to resolve the error
			// not this, this is not good enough
			// e.printStackTrace();
			
			// however, maybe the right thing to do is merely recording
			System.out.println(e.getMessage());
		}
		
		System.out.println("Normal operations continue here");
	}

}
