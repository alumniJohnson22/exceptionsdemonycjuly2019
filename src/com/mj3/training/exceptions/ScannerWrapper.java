package com.mj3.training.exceptions;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class ScannerWrapper implements AutoCloseable {

	private Scanner scanner;
	
	public ScannerWrapper () {
		 scanner = new Scanner(System.in);
	}
	
	@Override
	public void close() {
		// this is a no-op (no operation)
		// do not send this request to Scanner
	}

	public Pattern delimiter() {
		return scanner.delimiter();
	}

	public boolean equals(Object arg0) {
		return scanner.equals(arg0);
	}

	public Stream<MatchResult> findAll(Pattern arg0) {
		return scanner.findAll(arg0);
	}

	public Stream<MatchResult> findAll(String arg0) {
		return scanner.findAll(arg0);
	}

	public String findInLine(Pattern arg0) {
		return scanner.findInLine(arg0);
	}

	public String findInLine(String arg0) {
		return scanner.findInLine(arg0);
	}

	public String findWithinHorizon(Pattern arg0, int arg1) {
		return scanner.findWithinHorizon(arg0, arg1);
	}

	public String findWithinHorizon(String arg0, int arg1) {
		return scanner.findWithinHorizon(arg0, arg1);
	}

	public boolean hasNext() {
		return scanner.hasNext();
	}

	public boolean hasNext(Pattern arg0) {
		return scanner.hasNext(arg0);
	}

	public boolean hasNext(String arg0) {
		return scanner.hasNext(arg0);
	}

	public boolean hasNextBigDecimal() {
		return scanner.hasNextBigDecimal();
	}

	public boolean hasNextBigInteger() {
		return scanner.hasNextBigInteger();
	}

	public boolean hasNextBigInteger(int arg0) {
		return scanner.hasNextBigInteger(arg0);
	}

	public boolean hasNextBoolean() {
		return scanner.hasNextBoolean();
	}

	public boolean hasNextByte() {
		return scanner.hasNextByte();
	}

	public boolean hasNextByte(int arg0) {
		return scanner.hasNextByte(arg0);
	}

	public boolean hasNextDouble() {
		return scanner.hasNextDouble();
	}

	public boolean hasNextFloat() {
		return scanner.hasNextFloat();
	}

	public boolean hasNextInt() {
		return scanner.hasNextInt();
	}

	public boolean hasNextInt(int arg0) {
		return scanner.hasNextInt(arg0);
	}

	public boolean hasNextLine() {
		return scanner.hasNextLine();
	}

	public boolean hasNextLong() {
		return scanner.hasNextLong();
	}

	public boolean hasNextLong(int arg0) {
		return scanner.hasNextLong(arg0);
	}

	public boolean hasNextShort() {
		return scanner.hasNextShort();
	}

	public boolean hasNextShort(int arg0) {
		return scanner.hasNextShort(arg0);
	}

	public int hashCode() {
		return scanner.hashCode();
	}

	public IOException ioException() {
		return scanner.ioException();
	}

	public Locale locale() {
		return scanner.locale();
	}

	public MatchResult match() {
		return scanner.match();
	}

	public String next() {
		return scanner.next();
	}

	public String next(Pattern arg0) {
		return scanner.next(arg0);
	}

	public String next(String arg0) {
		return scanner.next(arg0);
	}

	public BigDecimal nextBigDecimal() {
		return scanner.nextBigDecimal();
	}

	public BigInteger nextBigInteger() {
		return scanner.nextBigInteger();
	}

	public BigInteger nextBigInteger(int arg0) {
		return scanner.nextBigInteger(arg0);
	}

	public boolean nextBoolean() {
		return scanner.nextBoolean();
	}

	public byte nextByte() {
		return scanner.nextByte();
	}

	public byte nextByte(int arg0) {
		return scanner.nextByte(arg0);
	}

	public double nextDouble() {
		return scanner.nextDouble();
	}

	public float nextFloat() {
		return scanner.nextFloat();
	}

	public int nextInt() {
		return scanner.nextInt();
	}

	public int nextInt(int arg0) {
		return scanner.nextInt(arg0);
	}

	public String nextLine() {
		return scanner.nextLine();
	}

	public long nextLong() {
		return scanner.nextLong();
	}

	public long nextLong(int arg0) {
		return scanner.nextLong(arg0);
	}

	public short nextShort() {
		return scanner.nextShort();
	}

	public short nextShort(int arg0) {
		return scanner.nextShort(arg0);
	}

	public int radix() {
		return scanner.radix();
	}

	public void remove() {
		scanner.remove();
	}

	public Scanner reset() {
		return scanner.reset();
	}

	public Scanner skip(Pattern arg0) {
		return scanner.skip(arg0);
	}

	public Scanner skip(String arg0) {
		return scanner.skip(arg0);
	}

	public String toString() {
		return scanner.toString();
	}

	public Stream<String> tokens() {
		return scanner.tokens();
	}

	public Scanner useDelimiter(Pattern arg0) {
		return scanner.useDelimiter(arg0);
	}

	public Scanner useDelimiter(String arg0) {
		return scanner.useDelimiter(arg0);
	}

	public Scanner useLocale(Locale arg0) {
		return scanner.useLocale(arg0);
	}

	public Scanner useRadix(int arg0) {
		return scanner.useRadix(arg0);
	}
	
	

}
