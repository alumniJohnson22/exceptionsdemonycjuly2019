package com.mj3.training.exceptions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.Scanner;

public class FNFExample {

	private void processAfterReadingFile() {
		System.out.println("process after reading the file.");

	}

	private void doWorkWithFile(String fileName) throws IOException {

		InputStream inStream = new FileInputStream(fileName);
		System.out.println("Normal processing of file continues here");
		inStream.close();

	}

	public static void main(String[] args) {
		FNFExample ex1 = new FNFExample();
		String fileName = ex1.getFNFromUser();
		NotAutoCloseableResource resource = new NotAutoCloseableResource();
		boolean workNotFinished = true;
		while (workNotFinished) {
			try {
				ex1.doWorkWithFile(fileName);
				ex1.processAfterReadingFile();
				workNotFinished = false;
			} catch (IOException | IllegalStateException e) {
				System.out.println(e.getMessage());
				fileName = ex1.getFNFromUser();
			}
			finally {
				resource.finishProcessing();
			}
		}
	}
	
	private void communicateToUser() {System.out.println("What file would you like to open?");		}

	private String getFNFromUser() {
		if (new Random().nextInt(3) % 3 == 0) {
			throw new IllegalStateException("This code is buggy.");
		}
		String fileName = "";
		communicateToUser();
		try (ScannerWrapper scanner = new ScannerWrapper()) {
			fileName = scanner.next();
		}
		return fileName;
	}

}
