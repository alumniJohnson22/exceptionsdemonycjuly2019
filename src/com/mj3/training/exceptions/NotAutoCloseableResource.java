package com.mj3.training.exceptions;

public class NotAutoCloseableResource {
	
	public void finishProcessing() {
		System.out.println("Not autoclosable, but, closing.");
	}

}
