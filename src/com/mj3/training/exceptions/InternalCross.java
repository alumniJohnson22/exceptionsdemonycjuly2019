package com.mj3.training.exceptions;

import java.util.Random;

public class InternalCross {

	public static void main(String[] args) throws InternalCrossException {
		if (new Random().nextInt(3) % 3 == 0) {
			throw new InternalCrossException("Could not cross with any bank clients");
		}
	}

	public static void doOtherThing(String[] args) {
		// DON'T both throw and catch the same exception, that's bad, and wasteful 
		// of the JVM's time
		try {
			if (new Random().nextInt(3) % 3 == 0) {
				throw new InternalCrossException("Could not cross with any bank clients");
			}
		} catch (InternalCrossException e) {

		}
	}

}
